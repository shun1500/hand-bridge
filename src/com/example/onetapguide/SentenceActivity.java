package com.example.onetapguide;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class SentenceActivity extends Activity {
//	private TextToSpeech tts;
	private static final int LIST_PICTURE_ID[] = {
			R.drawable.cut_02tanosii, R.drawable.cut_03okoru, R.drawable.cut_04kanasii,
			R.drawable.cut_05subarasii, R.drawable.cut_08hidoi1, R.drawable.cut_10omosiroi,
			R.drawable.cut_11okasii, R.drawable.cut_12odoroku1, R.drawable.cut_13syokku
	};

	private static final int LIST_PICTURE_ID1[] = {
		R.drawable.s0101_thank_you, R.drawable.s0102_sorry
	};

	private static final int LIST_PICTURE_ID2[] = {
		R.drawable.s0201_good_morning, R.drawable.s0202_good_evening, R.drawable.s0203_long_time_no_see
	};
	
	private static final int LIST_PICTURE_ID3[] = {
		R.drawable.s0301_im_lost, R.drawable.s0302_help, R.drawable.s0303_ambulance
	};

	private static final int LIST_PICTURE_ID4[] = {
		R.drawable.s0701_registration, R.drawable.s0702_stomachache, R.drawable.s0703_headache,
		R.drawable.s0704_fever,R.drawable.s0101_thank_you
	};

	private static final String LIST_JPN_STR[] = {
			"楽しい", "怒る", "悲しい",
			"素晴らしい", "ひどい", "面白い",
			"おかしい", "驚く", "ショック"
	};

	private static final String LIST_JPN_STR1[] = {
			"ありがとうございます", "ごめんなさい"
	};

	private static final String LIST_JPN_STR2[] = {
			"おはようございます", "こんばんわ", "おひさしぶりです"
	};

	private static final String LIST_JPN_STR3[] = {
			"道にまよいました", "助けてください", "救急車をお願いします"
	};

	private static final String LIST_JPN_STR4[] = {
			"受付をお願いします", "おなかが痛いです", "頭が痛いです", "熱があります", "ありがとうございました"
	};

	private static final String LIST_ENG_STR[] = {
			"Happy", "Angry", "Sad",
			"Great", "Terrible", "Interesting",
			"Funny", "Surprised", "Shock"
	};

	private static final String LIST_ENG_STR1[] = {
			"thank you", "sorry"
	};

	private static final String LIST_ENG_STR2[] = {
			"good morning", "good evening", "long time no see"
	};

	private static final String LIST_ENG_STR3[] = {
			"I'm lost", "Help", "call ambulance"
	};

	private static final String LIST_ENG_STR4[] = {
//			"I want to accept", "I have a stomachache", "I have a headache", "I have a fever", "thank you"
			"I want to registration", "I have a stomachache", "I have a headache", "I have a fever", "thank you"
	};

	private static final int PICTURE_ID[] = {
			R.id.imageView1, R.id.imageView2, R.id.imageView3, R.id.imageView4, R.id.imageView5,
			R.id.imageView6, R.id.imageView7, R.id.imageView8, R.id.imageView9
	};

	private EditText editText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO 自動生成されたメソッド・スタブ
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sentence_main);

		ListView listView = (ListView) findViewById(R.id.listView1);
		editText = (EditText) findViewById(R.id.editText1);
		int topPictureID = getIntent().getIntExtra("id", 0);
		
		//TODO ムービー用の仕方なし処理
		int loopStartNum = 0;
		int loopEndNum = 0;

		List<SentenceDTO> relatedDto = new ArrayList<SentenceDTO>();

		switch (topPictureID) {
		case R.id.imageView1:
			for (int i = 0; i < LIST_JPN_STR1.length; i++) {
				SentenceDTO dto = new SentenceDTO();
				dto.setPictureImageId(LIST_PICTURE_ID1[i]);
				dto.setJapaneseSentence(LIST_JPN_STR1[i]);
				dto.setEnglishSentence(LIST_ENG_STR1[i]);
				relatedDto.add(dto);
			}

			break;

		case R.id.imageView2:
			for (int i = 0; i < LIST_JPN_STR2.length; i++) {
				SentenceDTO dto = new SentenceDTO();
				dto.setPictureImageId(LIST_PICTURE_ID2[i]);
				dto.setJapaneseSentence(LIST_JPN_STR2[i]);
				dto.setEnglishSentence(LIST_ENG_STR2[i]);
				relatedDto.add(dto);
			}

			break;

		case R.id.imageView3:
			for (int i = 0; i < LIST_JPN_STR3.length; i++) {
				SentenceDTO dto = new SentenceDTO();
				dto.setPictureImageId(LIST_PICTURE_ID3[i]);
				dto.setJapaneseSentence(LIST_JPN_STR3[i]);
				dto.setEnglishSentence(LIST_ENG_STR3[i]);
				relatedDto.add(dto);
			}

			break;

		case R.id.imageView4:
		case R.id.imageView5:
		case R.id.imageView6:
		case R.id.imageView7:
		case R.id.imageView8:
		case R.id.imageView9:
			for (int i = 0; i < LIST_JPN_STR4.length; i++) {
				SentenceDTO dto = new SentenceDTO();
				dto.setPictureImageId(LIST_PICTURE_ID4[i]);
				dto.setJapaneseSentence(LIST_JPN_STR4[i]);
				dto.setEnglishSentence(LIST_ENG_STR4[i]);
				relatedDto.add(dto);
			}

			break;

		default:
			Log.e("OneTapGuide", "ありえないID");
			break;
		}

		//TODO これもサンプルのための一次処理
//		List<SentenceDTO> relatedDto = new ArrayList<SentenceDTO>();
//		for (int i = loopStartNum; i < loopEndNum; i++) {
////		for (int i = 0; i < LIST_JPN_STR.length; i++) {
//			SentenceDTO dto = new SentenceDTO();
//			dto.setPictureImageId(LIST_PICTURE_ID[i]);
//			dto.setJapaneseSentence(LIST_JPN_STR[i]);
//			dto.setEnglishSentence(LIST_ENG_STR[i]);
//			relatedDto.add(dto);
//		}

		//SentenceDTO型のカスタムアダプターを使用する
		CustomAdapter adapter = new CustomAdapter(
				SentenceActivity.this,
				R.layout.item_list,
				relatedDto);
		listView.setAdapter(adapter);

		//TextToSpeechのお試し
//		tts = new TextToSpeech(SentenceActivity.this, new OnInitListener() {
//			@Override
//			public void onInit(int status) {
//			}
//		});
//		tts.setLanguage(Locale.ENGLISH);
//		tts.speak("ENGLISH", TextToSpeech.QUEUE_FLUSH, null);

//		listView.setOnItemClickListener(new OnItemClickListener() {
//
//			@Override
//			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//				// TODO 自動生成されたメソッド・スタブ
//
//				//今は処理ないよ
//			}
//		});

//		editText.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				// TODO 自動生成されたメソッド・スタブ
//				editText.setFocusable(true);
//				editText.setFocusableInTouchMode(true);
//
//			}
//		});
	}

	public class CustomAdapter extends ArrayAdapter<SentenceDTO> {
		private int resource;

		public CustomAdapter(Context context, int resource, List<SentenceDTO> objects) {
			super(context, resource, objects);
			// TODO 自動生成されたコンストラクター・スタブ
			//リソースファイルは後で使うので、メンバ変数に保持しておく。
			this.resource = resource;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO 自動生成されたメソッド・スタブ
			/* レイアウトを生成する */
			convertView = View.inflate(getContext(), this.resource, null);

			/* 生成したViewから各パーツを取得する */
			ImageView pictureImageView = (ImageView) convertView.findViewById(R.id.pictureImageView);
			TextView japaneseSentence = (TextView) convertView.findViewById(R.id.japaneseSentence);
			TextView englishSentence = (TextView) convertView.findViewById(R.id.englishSentence);
			Typeface typeface = Typeface.createFromAsset(getResources().getAssets(), "rounded-x-mplus-1mn-regular.ttf");
			japaneseSentence.setTypeface(typeface);
			englishSentence.setTypeface(typeface);

			SentenceDTO item = getItem(position);

			int pictureImageId = item.getPictureImageId();
			pictureImageView.setImageResource(pictureImageId);

			japaneseSentence.setText(item.getJapaneseSentence());
			englishSentence.setText(item.getEnglishSentence());

			//一所懸命作ったViewを返す。
			return convertView;
		}

	}

}
