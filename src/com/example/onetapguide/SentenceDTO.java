package com.example.onetapguide;

public class SentenceDTO {
	private int pictureImageId;
	private String japaneseSentence;
	private String englishSentence;

	public SentenceDTO() {
		super();
		this.pictureImageId = 0;
		this.japaneseSentence = "";
		this.englishSentence = "";
	}

	public int getPictureImageId() {
		return pictureImageId;
	}

	public void setPictureImageId(int pictureImageId) {
		this.pictureImageId = pictureImageId;
	}

	public String getJapaneseSentence() {
		return japaneseSentence;
	}

	public void setJapaneseSentence(String japaneseSentence) {
		this.japaneseSentence = japaneseSentence;
	}

	public String getEnglishSentence() {
		return englishSentence;
	}

	public void setEnglishSentence(String englishSentence) {
		this.englishSentence = englishSentence;
	}
}
