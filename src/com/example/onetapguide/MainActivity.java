package com.example.onetapguide;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {
	private static final int PICTURE_ID[] = {
			R.id.imageView1, R.id.imageView2, R.id.imageView3, R.id.imageView4, R.id.imageView5,
			R.id.imageView6, R.id.imageView7, R.id.imageView8, R.id.imageView9
	};

	private List<ImageView> pictureList = new ArrayList<ImageView>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		TextView appliNameTxt = (TextView) findViewById(R.id.appliName);
		Typeface typeface = Typeface.createFromAsset(getResources().getAssets(), "rounded-x-mplus-1mn-regular.ttf");
		appliNameTxt.setTypeface(typeface);

		for (int i = 0; i < PICTURE_ID.length; i++) {
			pictureList.add((ImageView) findViewById(PICTURE_ID[i]));
			pictureList.get(i).setOnClickListener(this);
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO 自動生成されたメソッド・スタブ

		Intent intent = new Intent(this, SentenceActivity.class);
		intent.putExtra("id", v.getId());
		startActivity(intent);
	}

}
