package com.example.onetapguide;

import java.util.List;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CustomAdapter2 extends ArrayAdapter<SentenceDTO> {
	private int resource;

	public CustomAdapter2(Context context, int resource, List<SentenceDTO> objects) {
		super(context, resource, objects);
		// TODO 自動生成されたコンストラクター・スタブ
		//リソースファイルは後で使うので、メンバ変数に保持しておく。
		this.resource = resource;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO 自動生成されたメソッド・スタブ
		/* レイアウトを生成する */
		convertView = View.inflate(getContext(), this.resource, null);

		/* 生成したViewから各パーツを取得する */
		ImageView pictureImageView = (ImageView) convertView.findViewById(R.id.pictureImageView);
		TextView japaneseSentence = (TextView) convertView.findViewById(R.id.japaneseSentence);
		TextView englishSentence = (TextView) convertView.findViewById(R.id.englishSentence);
//		Typeface typeface = Typeface.createFromAsset(getResources().getAssets(), "rounded-x-mplus-1mn-regular.ttf");
//		japaneseSentence.setTypeface(typeface);
//		englishSentence.setTypeface(typeface);

		SentenceDTO item = getItem(position);

		int pictureImageId = item.getPictureImageId();
		pictureImageView.setImageResource(pictureImageId);

		japaneseSentence.setText(item.getJapaneseSentence());
		englishSentence.setText(item.getEnglishSentence());

		//一所懸命作ったViewを返す。
		return convertView;
	}

}
